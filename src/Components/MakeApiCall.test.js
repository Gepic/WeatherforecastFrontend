import { render, screen } from "@testing-library/react";
import MakeApiCall from "./MakeApiCall";

test("render app", async () => {
  render(<MakeApiCall />);
  const paragraphElement = screen.getByText(/Loading.../i);
  expect(paragraphElement).toBeInTheDocument();
});
